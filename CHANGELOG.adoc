= CHANGELOG

== `v0.3.0`

* Handles true type fonts and text streams (not just annotations)
* Handles borders where there is no longer a drawing file name by "visually" finding the drawing number based on the "DRAWING NUMBER" and "SHEET" text in the lower right corner
* Improve rung referencing consistent for drawings with an orientation of 270 deg
* General reworking of cargo cult programming and improving clarity and refactoring (more could be done...)
* Add `get_pdf` script to grab all drawings from a standard-designs folder structure
* Remove darker blue border around hyperlinks
* Decrease alpha to make hyperlinks less visually intrusive

== `v0.2.0`

* Handles older drawing borders (mid to late 2019) with drawing file name in border
* Handles drawings with text annotations

== `v0.1.0`

* A very early release, exact features lost in time and space.
