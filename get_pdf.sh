#!/bin/bash

PROJECT_DIR=/media/mulhollandd/KINGSTON/standard-designs
cd "${PROJECT_DIR%/*}"
outdir="output"; mkdir -p "$outdir"
find "$PROJECT_DIR" -type f -wholename '**/drawings/*.pdf' -printf "%p\0${outdir}/%P\0" |
  awk 'BEGIN{FS="/";RS=ORS="\0"}
       NR%2||NF==2 {print; next}
       {gsub("/","#"); sub("#","/#"); print}' |
    xargs -0 -n2 cp -T
