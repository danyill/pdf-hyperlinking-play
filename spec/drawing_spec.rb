# frozen_string_literal: true

require 'fileutils'

require_relative '../lib/tp-pdf-links'

BASE_PATH = File.dirname(__FILE__)

LINE_TEST_FILE = 'fixtures/Line_Prot_Test.pdf'
TX_TEST_FILE = 'fixtures/Tx_Prot_Test.pdf'
FEEDER_TTF_BORDER = 'fixtures/Feeder_2020_TTF_Border.pdf'
DC_NO_TTF_BORDER = 'fixtures/DC_2020_Non_TTF_Border.pdf'

describe Drawing do
  let(:dc_line) do
    DrawingCatalog.new(File.join(BASE_PATH, LINE_TEST_FILE), debug:false, dryrun: true)
  end

  let(:dc_tx) do
    DrawingCatalog.new(File.join(BASE_PATH, TX_TEST_FILE), debug:false, dryrun: true)
  end

  let(:dc_feeder) do
    DrawingCatalog.new(File.join(BASE_PATH, FEEDER_TTF_BORDER), debug:false, dryrun: true)
  end

  let(:dc_dc) do
    DrawingCatalog.new(File.join(BASE_PATH, DC_NO_TTF_BORDER), debug:false, dryrun: true)
  end

  context 'after loading a page it' do
    it 'correctly identifies the page rotation at 0 deg' do
      firstPage = dc_line.drawings[0]
      expect(firstPage.page_rotation).to eq(0)
    end

    it 'correctly identifies the page orientation as landscape' do
      firstPage = dc_line.drawings[0]
      expect(firstPage.page_orientation).to eq(:landscape)
    end

    it 'correctly identifies the page rotation at 270 deg' do
      page = dc_tx.drawings[0]
      expect(page.page_rotation).to eq(270)
    end

    it 'correctly identifies line protection rungs on page 1' do
      page = dc_line.drawings[0]
      rungs = [*41..60]
      rungs = rungs.map{ |x| x.to_s }
      expect(page.rungs.keys).to eq(rungs)
    end

    it 'correctly identifies line protection rungs on page 2' do
      page = dc_line.drawings[1]
      rungs = [*181..200]
      rungs = rungs.map{ |x| x.to_s }
      expect(page.rungs.keys).to eq(rungs)
    end

    it 'correctly identifies no rungs for tx protection on page 1' do
      page = dc_tx.drawings[0]
      rungs = []
      expect(page.rungs.keys).to eq(rungs)
    end

    it 'correctly identifies tx protection rungs on page 2' do
      page = dc_tx.drawings[1]
      rungs = [*1..20]
      rungs = rungs.map{ |x| x.to_s }
      expect(page.rungs.keys).to eq(rungs)
    end

    it 'correctly identifies tx protection rungs on page 3' do
      page = dc_tx.drawings[2]
      rungs = [*101..120]
      rungs = rungs.map{ |x| x.to_s }
      expect(page.rungs.keys).to eq(rungs)
    end

    it 'correctly identifies the filename for tx on page 1' do
      page = dc_tx.drawings[0]
      expect(page.drawingFileName).to eq('TP108344_1_0.DWG')
    end

    it 'correctly identifies the drawing number for tx on page 1' do
      page = dc_tx.drawings[0]
      expect(page.drawingNum).to eq('TP108344')
    end

    it 'correctly identifies the sheet number for tx on page 1' do
      page = dc_tx.drawings[0]
      expect(page.sheet).to eq('1')
    end

    it 'correctly identifies the filename for line on page 1' do
      page = dc_line.drawings[0]
      expect(page.drawingFileName).to eq('TP108421_4_0.DWG')
    end

    it 'correctly identifies the drawing number for line on page 1' do
      page = dc_line.drawings[0]
      expect(page.drawingNum).to eq('TP108421')
    end

    it 'correctly identifies the sheet number for line on page 1' do
      page = dc_line.drawings[0]
      expect(page.sheet).to eq('4')
    end

    it 'correctly identifies rung references for line protection on page 1' do
      references = {"[189]"=>"189", "[224]"=>"224", "[187]"=>"187", "[202]"=>"202", "[228]"=>"228", "[223]"=>"223", "[206]"=>"206", "[191]"=>"191", "[192]"=>"192", "[193]"=>"193", "[194]"=>"194", "[195]"=>"195", "[196]"=>"196", "[197]"=>"197", "[198]"=>"198", "[199]"=>"199", "[207]"=>"207", "[208]"=>"208", "[209]"=>"209", "[210]"=>"210", "[211]"=>"211", "[212]"=>"212", "[213]"=>"213", "[214]"=>"214", "[215]"=>"215", "[203]"=>"203", "[204]"=>"204", "[181]"=>"181", "[218]"=>"218", "Continued at [47] & [51]"=>"47", "Continued at [53] & [56]"=>"53", "[276]"=>"276", "[299]"=>"299", "[14]"=>"14"}
      page = dc_line.drawings[0]
      expect(page.rungRefs).to eq(references)
    end

    it 'correctly identifies rung references for tx protection on page 3' do
      references = {"[386]"=>"386", "[393]"=>"393", "[331]"=>"331", "[329]"=>"329", "[3]"=>"3", "[7]"=>"7", "[11]"=>"11", "[37]"=>"37", "[28]"=>"28", "[16]"=>"16", "[46]"=>"46", "[328]"=>"328", "[398]"=>"398", "[384]"=>"384", "[366]"=>"366", "[381]"=>"381", "[327]"=>"327", "[403]"=>"403", "[362]"=>"362", "[332]"=>"332", "[409]"=>"409", "[342]"=>"342", "[344]"=>"344", "[346]"=>"346", "[348]"=>"348", "[350]"=>"350", "[356]"=>"356", "[357]"=>"357", "[358]"=>"358", "[378]"=>"378", "[418]"=>"418", "[10]"=>"10", "[38]"=>"38", "[27]"=>"27", "[17]"=>"17", "[47]"=>"47", "[420]"=>"420"}
      page = dc_tx.drawings[2]
      expect(page.rungRefs).to eq(references)
    end

    it 'creates for tx protection on page 3 the right number of links' do
      references = {"[386]"=>"386", "[393]"=>"393", "[331]"=>"331", "[329]"=>"329", "[3]"=>"3", "[7]"=>"7", "[11]"=>"11", "[37]"=>"37", "[28]"=>"28", "[16]"=>"16", "[46]"=>"46", "[328]"=>"328", "[398]"=>"398", "[384]"=>"384", "[366]"=>"366", "[381]"=>"381", "[327]"=>"327", "[403]"=>"403", "[362]"=>"362", "[332]"=>"332", "[409]"=>"409", "[342]"=>"342", "[344]"=>"344", "[346]"=>"346", "[348]"=>"348", "[350]"=>"350", "[356]"=>"356", "[357]"=>"357", "[358]"=>"358", "[378]"=>"378", "[418]"=>"418", "[10]"=>"10", "[38]"=>"38", "[27]"=>"27", "[17]"=>"17", "[47]"=>"47", "[420]"=>"420"}
      page = dc_tx.drawings[2]
      expect(page.rungRefs).to eq(references)
    end

    it 'creates the right qty links for tx protection on page 1' do
      totalLinks = 4
      page = dc_tx.drawings[0]
      expect(page.newLinks.length).to eq(totalLinks)
    end

    it 'creates the right qty of links for tx protection on page 2' do
      totalLinks = 7
      page = dc_tx.drawings[1]
      expect(page.newLinks.length).to eq(totalLinks)
    end

    it 'creates the right qty of links for tx protection on page 3' do
      totalLinks = 29
      page = dc_tx.drawings[2]
      expect(page.newLinks.length).to eq(totalLinks)
    end

    it 'creates the right qty of links for line protection on page 1' do
      totalLinks = 15
      page = dc_line.drawings[0]
      expect(page.newLinks.length).to eq(totalLinks)
    end

    it 'creates the right qty of links for line protection on page 2' do
      totalLinks = 2
      page = dc_line.drawings[1]
      expect(page.newLinks.length).to eq(totalLinks)
    end

    it 'correctly identifies the drawing/sheet for the new 2020 non-TTF drawing border' do
      page = dc_feeder.drawings[0]
      expect(page.drawingNum).to eq('TP107228')
      expect(page.sheet).to eq('2B')
      page = dc_feeder.drawings[1]
      expect(page.drawingNum).to eq('TP107228')
      expect(page.sheet).to eq('3')
    end

    it 'correctly identifies the drawing/sheet for the new 2020 TTF drawing border' do
      page = dc_dc.drawings[0]
      expect(page.drawingNum).to eq('TP114815')
      expect(page.sheet).to eq('1')
      page = dc_dc.drawings[1]
      expect(page.drawingNum).to eq('TP114815')
      expect(page.sheet).to eq('4')
      page = dc_dc.drawings[2]
      expect(page.drawingNum).to eq('TP114815')
      expect(page.sheet).to eq('5')
    end

  end
end
