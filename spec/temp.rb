require 'hexapdf'

BASE_PATH = File.dirname(__FILE__)

LINE_TEST_FILE = 'fixtures/Line_Prot_Test.pdf'
TX_TEST_FILE = 'fixtures/Tx_Prot_Test.pdf'

def showAnnotationRects(fileName)

    doc = HexaPDF::Document.open(File.join(BASE_PATH, fileName))

    puts doc.pages[0][:Rotate] # rotation

    doc.pages[0][:Annots].each do |x|
        a = doc.deref(x)
        if a.key? :Rect
            puts a[:Rect].inspect
        end
    end

end

showAnnotationRects(LINE_TEST_FILE)

showAnnotationRects(TX_TEST_FILE)
