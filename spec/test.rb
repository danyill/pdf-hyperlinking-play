# frozen_string_literal: true

require 'fileutils'

require_relative '../lib/tp-pdf-links'

BASE_PATH = File.dirname(__FILE__)

LINE_TEST_FILE = 'fixtures/Line_Prot_Test.pdf'
TX_TEST_FILE = 'fixtures/Tx_Prot_Test.pdf'
FEEDER_TTF_BORDER = 'fixtures/Feeder_2020_TTF_Border.pdf'
DC_NON_TTF_BORDER = 'fixtures/DC_2020_Non_TTF_Border.pdf'
FEEDER_INVENTOR_PANELS = 'fixtures/Feeder_Inventor_Panel.pdf'

# dc1 = DrawingCatalog.new(File.join(BASE_PATH, TX_TEST_FILE), debug:true, verbose:true, canvasLinks: true)

# dc2 = DrawingCatalog.new(File.join(BASE_PATH, LINE_TEST_FILE), debug:true, verbose:true, canvasLinks: true)

# f1 = DrawingCatalog.new(File.join(BASE_PATH, FEEDER_TTF_BORDER), debug:true, verbose:true, canvasLinks: true)

# f2 = DrawingCatalog.new(File.join(BASE_PATH, DC_NON_TTF_BORDER), debug:true, verbose:true, canvasLinks: true)

# f3 = DrawingCatalog.new(File.join(BASE_PATH, FEEDER_INVENTOR_PANELS), debug:true, verbose:true, canvasLinks: true)

f3 = DrawingCatalog.new(File.join(BASE_PATH, 'fixtures/Example_AutoCAD_Inventor_Text.pdf'), debug:true, verbose:true, canvasLinks: true)



# require 'pp'
# require 'pry' # must install the gem... but you ALWAYS want pry installed anyways
# Pry::ColorPrinter.pp(dc.rungLookup)