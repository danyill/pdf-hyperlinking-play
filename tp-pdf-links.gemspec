# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'tp_pdf_links'
  s.version     = '0.3.0'
  s.date        = '2020-06-01'
  s.summary     = 'Add links to circuit diagram PDFs'
  s.description = 'A gem to post-process certain pdf diagrams and place links between pages'
  s.authors     = ['Daniel Mulholland']
  s.email       = 'dan.mulholland@gmail.com'
  s.files       = Dir['lib/*.rb']
  s.executables << 'tp-pdf-links'
  s.homepage =
    'https://rubygems.org/gems/tp_pdf-links'
  s.license = 'MIT'
  s.add_runtime_dependency('hexapdf', '0.11.5')
  s.add_development_dependency('rspec', '~>3.9')
end
