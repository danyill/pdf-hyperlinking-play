require 'hexapdf'

class DrawingTextProcessor < HexaPDF::Content::Processor
  attr_reader :textInformation

  def initialize(page, drawing)
    super()
    @drawing = drawing
    @textInformation = []
    @canvas = page.canvas(type: :overlay)
  end

  def show_text(str)

    begin
      boxes = decode_text_with_positioning(str)
    rescue HexaPDF::Error
      return
    end

    return if boxes.string.empty?

    coords = [*boxes.upper_left, *boxes.lower_right] # 'l t r b for angle == 0

    transformation = self.graphics_state.tm.to_a()
    @textInformation << [decode_text(str), @drawing.getAbsRect(coords), 'text', transformation]
    # puts transformation.to_s + ' ' + decode_text(str) + ' ' + @drawing.page.index.to_s

  end
  alias :show_text_with_positioning :show_text

end