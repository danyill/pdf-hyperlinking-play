module Coords

  def getAbsRect(rect, ang=z)
    if !rect.instance_of?(Array)
      rect = rect.value
    end
    if ang == 270
      # hexapdf provides r b l t we translate to l t r b
      rect = [rect[2], rect[1], rect[0], rect[3]]
    end
    lt = toAbsCoord(rect.slice(0,2), ang)
    rb = toAbsCoord(rect.slice(2,4), ang)
    return [lt[0], lt[1], rb[0], rb[1],
            (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
  end

  def getRelRect(rect,ang=@page_rotation)
    lt = toRelCoord(rect.slice(0,2), ang)
    rb = toRelCoord(rect.slice(2,4), ang)
    # puts lt, rb
    if ang==0
      # l t r b
      return [lt[0], lt[1], rb[0], rb[1],
             (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
    elsif ang==270
      # r t l b
      return [rb[0], lt[1], lt[0], rb[1],
              (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
    end
  end

  def toAbsCoord(coord, ang=@page_rotation)
    # always from angle to an angle of zero degrees
    x, y = coord
    if ang == 0
      return [x, y]
    elsif ang == 270
      new_x = @page_width - y
      new_y = x
      return [new_x, new_y]
    end
  end

  def toRelCoord(coord, ang=@page_rotation)
    x, y = coord
    if ang == 0
      return [x, y]
    elsif ang == 270
      new_x = y
      new_y = @page_width - x
      return [new_x, new_y]
    end
  end

end