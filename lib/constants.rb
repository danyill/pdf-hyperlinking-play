# encoding: utf-8
# frozen_string_literal: true

class C

    # Matching of drawing references and filenames
    DRAWINGFILENAME = /^T.*\.DWG$/u
    DRAWINGREFERENCE = %r{^\*?(TX[0-9]+|TP[0-9]+|DWG[A-Z@$]*[0-9@]*[A-Z@$]*)\*?(/[0-9A-Z]+)?$}u

    # Matching of different rung styles
    RUNG1 = /\[([0-9]+)\]/u
    RUNG2 = /\[(\*?[A-Z@$]+[0-9@$]*\*?)?(, )?([0-9]+)+(-)?([0-9]*)?\]/u # [TP1234, 4] or  [TP1234, 4-6] T1 AVR [TX41170, 11]
    RUNG3 = /\[([0-9]+)\-[0-9]+\]/u # [100-122]
    RUNG4 = %r{^\*?(TX[0-9]+|TP[0-9]+|DWG[A-Z@$]*[0-9@]*[A-Z@$]*)\*?(/[0-9A-Z]+)?$}u # ^DWG/3$
    RUNG5 = /\[[0-9]+, [0-9]+\]/u # [100, 140]

    # Portion of the page to search for page rungs
    MAX_RUNG_TOP_FACTOR = 0.95
    # Maximum rung text height
    MAX_RUNG_HEIGHT_FACTOR = 0.012

    # Colors
    NO_COLOR = [0, 0, 0]
    RUNG_COLOR = [1, 0, 0]
    FAKE_RUNG_COLOR = [0.5, 0.2, 0.2]
    REFERENCE_COLOR = RUNG_COLOR
    RUNG_REF_COLOR = [0, 0.5, 0]
    FILENAME_COLOR = [0.7, 0.7, 0.2]
    LINK_COLOR = [0.125, 0.5, 1]
    LINK_RECT_COLOR = LINK_COLOR #[32, 128, 32]

    SEARCH_DWG_SHEET_COLOR = [0, 0, 0.5]

    # Radius for rectangles created
    RECT_RADIUS = 0
    RECT_WIDTH = 2

end
