# encoding: utf-8
# frozen_string_literal: true

require_relative 'constants'
require_relative 'drawingTextProcessor'

class Drawing
  attr_reader :rungs, :rungRefs, :drawingNum, :sheet, :page, :index,
              :page_orientation, :page_rotation, :drawingFileName,
              :drawingNum, :sheet, :rungRefs, :newLinks

  def initialize(doc, page, debug, verbose, canvasLinks, fileName)
    @verbose = verbose
    @debug = debug
    @canvasLinks = canvasLinks
    @fileName = fileName

    @index = page.index
    @doc = doc
    @page = page
    @canvas = @page.canvas(type: :overlay)
    @annots = @page[:Annots] || []

    @drawingFileName = nil
    @drawingNum = nil
    @sheet = nil
    @rungs = {}
    @rungRefs = {}
    # @drawingRefs = {}
    @newLinks = []
    @boxes = []

    @textInformation = []
    @annotInformation = []

    @page_orientation = @page.orientation
    @page_rotation = @page[:Rotate]
    if @page_rotation == 0
      @page_width = @page.box.value[2]
      @page_height = @page.box.value[3]
    elsif @page_rotation == 270
      @page_width = @page.box.value[3]
      @page_height = @page.box.value[2]
    else
      puts 'ERROR: Unknown page rotation: ' + @page_rotation.to_s + ' in file: ' + @fileName
      return
    end

    if @verbose
      puts 'Orientation ' + @page_orientation.to_s + \
           ' H:' + @page_height.to_s + ' W: ' + \
           @page_width.to_s + ' Rotation: ' + @page_rotation.to_s  + ' in file: ' + @fileName
    end

    getAnnotInformation
    getDrawingAndSheetFromFilename
    getTextInformation
    getVisuallyDrawingAndSheetNum(@textInformation)
    getVisuallyDrawingAndSheetNum(@annotInformation)

    getSelectedAnnotations

  end

  def getTextInformation
    @processor = DrawingTextProcessor.new(page, self)
    @page.process_contents(@processor)
    @textInformation = @processor.textInformation
  end

  def getAnnotInformation
    @annots.each do |annot|
      right, bottom, left, top = annot[:Rect] # at angle zero, r b l t
      coords = [left, top, right, bottom]
      # %%U seen in some drawings for reasons which are unclear
      textContent = fixTextValue(annot.value[:Contents].dup)
      if !textContent.nil?
        textContent = textContent.sub('%%U','').sub('%%u','')
        @annotInformation << [textContent, getAbsRect(coords), 'annot']
      end
    end
  end

  # Public: Look for drawing number and sheet based on position and nearby  on the diagram.
  #
  # Iterate across objects looking for positioning in the bottom right 15%. Locate the drawing number by finding the
  # the text "DRAWING NUMBER" and looking beneath that and doing the same for revision. If found and the drawing
  # number and sheet are not already set, set them.
  #
  # Does not return anything
  def getVisuallyDrawingAndSheetNum dataSrc
    corner_factor = 0.10
    parts = {}
    dataSrc.each do |val|
      text, coords, type = val
      if ['DRAWING NUMBER', 'SHEET', 'REVISION'].include?(text) &&
        coords != nil &&
        coords[0] > (@page_width * corner_factor) &&
        coords[1] < (@page_height * (1-corner_factor)) # must be in the bottom right corner

          parts[text] = coords
      end
    end
    fnd_dn = nil
    fnd_sht = nil

    # coping with the ttf border whose text is different aspects and sizes
    factor = 0
    if dataSrc.length != nil && dataSrc.length > 0 && dataSrc[0][2] == 'text'
      factor = 0.4
    end

    if parts.key? 'DRAWING NUMBER'
        dnLeft, dnTop, dnRight, dnBottom, dnWidth, dnHeight = parts['DRAWING NUMBER']
        fnd_dn = [dnLeft - 0.65*dnWidth, dnBottom+0.5*dnHeight,
                  dnRight + 0.65*dnWidth, dnBottom-(1.8+factor)*dnHeight]
    end
    if parts.key? 'SHEET'
      shLeft, shTop, shRight, shBottom, shWidth, shHeight= parts['SHEET']
      fnd_sht = [shLeft - 0.2*shWidth, shBottom+0.5*shHeight,
                  shRight + 0.2*shWidth, shBottom-(1.8+factor)*shHeight]
    end

    drawRectfromCoords(fnd_dn, C::SEARCH_DWG_SHEET_COLOR, 'annot', []) if !fnd_dn.nil? &&  @debug
    drawRectfromCoords(fnd_sht, C::SEARCH_DWG_SHEET_COLOR, 'annot', []) if !fnd_sht.nil? && @debug

    drawingNum = nil
    sheet = nil

    dataSrc.each do |val|
      text, coords, type = val
      if fnd_dn != nil && rectWithin(fnd_dn, coords)
        drawingNum = text
        puts 'Page ' + (@index+1).to_s + ' ' + 'Drawing number located: ' + \
             fnd_dn.to_s + ' ' + coords.to_s + ' = ' + \
             drawingNum.to_s + ' type: ' + type if @verbose && drawingNum == text

      elsif fnd_sht != nil && rectWithin(fnd_sht, coords)
        sheet = text
        puts 'Page ' + (@index+1).to_s + ' ' + 'Sheet number located: ' + \
             fnd_sht.to_s + ' ' + coords.to_s + ' = ' + \
             sheet.to_s + ' type: ' + type if @verbose && sheet == text
      end

      if drawingNum != nil && sheet != nil
        break
      end

    end

    if @drawingNum == nil and drawingNum != nil
      @drawingNum = drawingNum
    end
    if @sheet == nil and sheet != nil
      @sheet = sheet
    end

  end

  # Public: Plots on the canvas a rectangle.
  #
  # Takes into account the page rotation and coordinate system.
  #
  # coord - coordinates l t r b as an [Array]
  # color - RGB color defined by hexapdf
  #
  def drawRectfromCoords(coord=nil, color, type, transformation)
    # canonically l t r b
    return false if coord == nil
    if type == nil
      type == 'annot'
    end
    @canvas.stroke_color = color
    left, top, right, bottom = coord
    width = (right-left).abs
    height = (top-bottom).abs

    left, top, right, bottom = getRelRect(coord)
    if @page_rotation == 0
      if type == 'annot'
        @canvas.rectangle(left, bottom, width, height, radius: 0)
      elsif type == 'text' and transformation.slice(0,4) == [0, 1, -1, 0]
        # true text which is oriented at 90 degrees
        @canvas.rectangle(left, top, width, height, radius: 0)
      else
        @canvas.rectangle(left, bottom, width, height, radius: 0)
      end
    elsif @page_rotation == 270
      @canvas.rectangle(left, bottom, height, width, radius: 0)
    end
    @canvas.stroke
  end

  # Public: Calculates whether rectB is inside rectA
  #
  # Receives coordinates left, top, right bottom for rectA and rectB
  # rectA - coordinates l t r b as an [Array]
  # rectB - coordinates l t r b as an [Array]
  #
  # Returns a [Boolean] indicating whether rectA is inside rectB
  def rectWithin(rectA, rectB)
    leftA, topA, rightA, bottomA = rectA
    leftB, topB, rightB, bottomB = rectB
    begin
      return leftB >= leftA && rightB <= rightA && \
           bottomB >= bottomA && topB <= topA
    rescue StandardError
      return false
    end
  end

  # Public: Calculates absolute coordinate sfor a rectangle
  #
  # Also calculates width and height.
  #  * x increases from the left
  #  * y increase from the bottom
  #
  # rect - coordinates l t r b as an [Array]
  # ang - the current rotation which needs to be transferred to angle 0
  #
  # Returns a [Array] indicating [l t r b w h]
  def getAbsRect(rect, ang=@page_rotation)
    # canonically in l t r b w h
    if !rect.instance_of?(Array)
      rect = rect.value # l t r b
    end
    if ang == 270
      # hexapdf provides r b l t we translate to l t r b
      rect = [rect[2], rect[1], rect[0], rect[3]]
    end
    lt = toAbsCoord(rect.slice(0,2), ang)
    rb = toAbsCoord(rect.slice(2,4), ang)
    return [lt[0], lt[1], rb[0], rb[1],
            (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
  end

  # Public: Calculates coordinates for a rectangle relative to the page rotation
  #
  # Suitable for create a rectangle on the canvas
  # See the image in the docs folder for information on this. It's tricky.
  #
  # rect - coordinates l t r b as an [Array]
  # ang - the current rotation which needs to be transferred to angle 0
  #
  # Returns a [Array] indicating [l t r b w h]
  def getRelRect(rect,ang=@page_rotation)
    # puts rect.to_s, ang
    lt = toRelCoord(rect.slice(0,2), ang)
    rb = toRelCoord(rect.slice(2,4), ang)
    if ang==0
      # l t r b w h
      return [lt[0], lt[1], rb[0], rb[1],
             (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
    elsif ang==270
      # r t l b w h
      return [rb[0], lt[1], lt[0], rb[1],
              (rb[0]-lt[0]).abs, (lt[1]-rb[1]).abs]
    end
  end

  # Public: Calculates absolute coordinates for a point relative to the page rotation
  #
  # rect - coordinates x y as an [Array]
  # ang - the current rotation which needs to be transferred to angle 0
  #
  # Returns a [Array] indicating [x y]
  def toAbsCoord(coord, ang=@page_rotation)
    # always from angle to an angle of zero degrees
    x, y = coord
    if ang == 0
      return [x, y]
    elsif ang == 270
      new_x = @page_width - y
      new_y = x
      return [new_x, new_y]
    end
  end

  # Public: Calculates coordinates relative to the page rotation
  #
  # rect - coordinates x y as an [Array]
  # ang - the current rotation which needs to be transferred to angle 0
  #
  # Returns a [Array] indicating [x y]
  def toRelCoord(coord, ang=@page_rotation)
    x, y = coord
    if ang == 0
      return [x, y]
    elsif ang == 270
      new_x = y
      new_y = @page_width - x
      return [new_x, new_y]
    end
  end

  def getRect(a)
    # this deals with cases not handled in HexaPDF yet
    # It may not handle angles correctly...
    if a.instance_of?(Array)
      left, top, right, bottom = a
    elsif a.key? :Rect && a[:Rect].instance_of?(Array)
      right, bottom, left, top = *a[:Rect]
    elsif a.value[:Rect] != nil && a.value[:Rect].instance_of?(Array)
      right, bottom, left, top = a.value[:Rect]
    elsif a.value[:Rect].value != nil && a.value[:Rect].value.instance_of?(Array)
      left, bottom, right, top = a.value[:Rect].value
    end
    begin
      width = (right - left).abs
      height = (top - bottom).abs
    rescue StandardError
      width = nil
      height = nil
    end
    return [left, bottom, right, top, width, height]
  end

  def fixTextValue(val)
    return nil if val == nil
    return val.force_encoding(Encoding::UTF_8).encode(Encoding::UTF_8,
      invalid: :replace, undef: :replace, replace: '').delete("\u0000")
  end

  def getDrawingAndSheetFromFilename
    @annotInformation.each do |val|
      text, coords, type, transformation = val
      if text.strip.upcase.match? C::DRAWINGFILENAME
        @drawingFileName = text.strip
        @drawingNum, @sheet = text.strip.split('_')
        puts 'Drawing found from file name : ' + @drawingNum + ' sheet: ' + @sheet if @verbose

        if @debug
          drawRectfromCoords(coords, C::FILENAME_COLOR, type, transformation)
        end

      end
    end
  end

  def getSelectedAnnotations
    (@annotInformation + @textInformation).each do |val|
      text, coords, type, transformation = val
      left, bottom, right, top, width, height = coords

      if width == nil or height == nil
        next
      end

      if text.numeric? && (top > @page_height * C::MAX_RUNG_TOP_FACTOR && \
        height > C::MAX_RUNG_HEIGHT_FACTOR * @page_height)

        # TODO: somewhat dirty hack
        left_coord = getRelRect(coords)[2] # right for ang=0
        top_coord = getRelRect(coords)[3] # top

        stroke_color = C::REFERENCE_COLOR
        # no repeated rungs, this improves our chances slightly because
        # the rung annotations are only very slightly smaller than the
        # border references
        if !@rungs.key?(text)
          @rungs[text] = [left_coord, top_coord, coords]
          puts 'Rung: ' + text.ljust(5) + ' ' + 'l:' + left_coord.to_s.ljust(5) +  \
               ' t:' + top_coord.to_s.ljust(5) if @verbose
          stroke_color = C::RUNG_COLOR if @debug
        else
          stroke_color = C::FAKE_RUNG_COLOR if @debug
        end

      elsif text.match? C::DRAWINGREFERENCE
        stroke_color = C::REFERENCE_COLOR if @debug
      elsif text.match?(C::RUNG1) || text.match?(C::RUNG2) || text.match?(C::RUNG3) || \
            text.match?(C::RUNG4) || text.match?(C::RUNG5)
        if match = text.match(C::RUNG1)
          @rungRefs[text] = match.captures[0]
        end
        stroke_color = C::RUNG_REF_COLOR if @debug
      else
        noMatch = true
      end

      if @debug && !noMatch
        drawRectfromCoords(coords, C::FILENAME_COLOR, type, transformation)
      end

    end
  end

  # Public: create hyperlinks on drawing references.
  #
  # Create Hyperlinks based on rung lookup.
  #
  # Does not return anything
  def makeLinks(rungLookup, drawingAndSheetLookup, _drawingCatalog)
    (@annotInformation + @textInformation).each do |val|
      text, coords, type, transformation = val
      left, bottom, right, top, width, height = coords
      # if @page_rotation ==
      # puts .to_s
      objRect = HexaPDF::Rectangle.new(getRelRect(coords).slice(0,4))
      # [left, bottom, right, top])
      # elsif @page_rotation == 270  # r t l b w h
      #   objRect = HexaPDF::Rectangle.new([right, top, left, bottom])
      # else
      #   puts 'Page rotation not supported: ' + @page_rotation
      # end

      pageNo = nil
      matchFound = nil
      drawingFound = @drawingNum

      if text.match?(C::RUNG1) || text.match?(C::RUNG2) || text.match?(C::RUNG3) || \
        text.match?(C::RUNG4) || text.match?(C::RUNG5)
        # order of processing is important because the regexes overlap...
        if match = text.match(C::RUNG1)
          matchFound = match.captures[0]
        elsif match = text.match(C::RUNG3)
          matchFound = match.captures[0]
        elsif match = text.match(C::RUNG2)
          matchFound = match.captures[2]
          drawingFound = match.captures[0]
        end
      end

      pageIndex, rleft, rtop, ann = rungLookup[[drawingFound, matchFound]]
      if matchFound and pageIndex != nil and drawingFound != nil

        puts (@page.index + 1).to_s + ' :: ' + @drawingNum.to_s + '  ' + \
            text.to_s.ljust(10) + ' ' + ' :' + matchFound.to_s.ljust(4) + ': ' + pageNo.to_s + \
            ' Rect: ' + objRect.to_s.ljust(25) + 'RefTo: p. ' + \
            (pageIndex+1).to_s + \
            ' L: ' + rleft.to_s.ljust(4) + ' T:' + rtop.to_s.ljust(4) if @verbose

        # get hyperlinks coordinates
        min_left = nil
        min_top = nil
        if @page_rotation == 0
          min_left = getRelRect(ann)[0]
          min_top = @page_height
        elsif @page_rotation == 270
          min_left = @page_height
          min_top = getRelRect(ann)[1]
        end

        # rectangle with border
        @boxes << @doc.add({Type: :Annot, Subtype: :Square, Rect: objRect, F: 0b000,
           C: C::LINK_COLOR, Border: [0, 0, 0], IC: C::LINK_COLOR, CA: 0.1})

        # hyperlink
        @newLinks << @doc.add({Type: :Annot, Subtype: :Link, Rect: objRect, F: 0b000,
          A: { Type: :Action, S: :GoTo,
          D: [@doc.pages[pageIndex], :XYZ, min_left, min_top, nil] },
          C: C::LINK_COLOR, Border: [0, 0, 0]})

        foundMatch = true
      end
      # puts drawingAndSheetLookup.inspect
      if match = text.match(C::DRAWINGREFERENCE)
        dRefs = text.split('/')
        refs = nil
        if drawingAndSheetLookup.key? dRefs
          refs = dRefs
        elsif dRefs.length == 1 && drawingAndSheetLookup.key?([dRefs[0], '1'])
          # if you can't find it, match to the first sheet
          refs = [dRefs[0], '1']
        elsif dRefs.length == 1 && drawingAndSheetLookup.key?([dRefs[0], '-'])
          # if you can't find it, match to a drawing with no sheets
          refs = [dRefs[0], '-']
        elsif dRefs.length == 1 && drawingAndSheetLookup.key?([dRefs[0], nil])
          # if you can't find it, match a drawing with no sheet identified
          refs = [dRefs[0], nil]
        elsif dRefs.length > 1 && drawingAndSheetLookup.key?([dRefs[0], dRefs[1]])
          refs = [dRefs[0], dRefs[1]]
        # elsif you can match an A, B, C, D sheet...
        # TODO: this should be rewritten in a concise manner.
        elsif dRefs.length > 1 && drawingAndSheetLookup.key?([dRefs[0],  dRefs[1] + 'A'])
          refs = [dRefs[0], dRefs[1] + 'A']
        elsif dRefs.length > 1 && drawingAndSheetLookup.key?([dRefs[0],  dRefs[1] + 'B'])
          refs = [dRefs[0], dRefs[1] + 'B']
        elsif dRefs.length > 1 && drawingAndSheetLookup.key?([dRefs[0],  dRefs[1] + 'C'])
          refs = [dRefs[0], dRefs[1] + 'C']
        elsif dRefs.length > 1 && drawingAndSheetLookup.key?([dRefs[0],  dRefs[1] + 'D'])
          refs = [dRefs[0], dRefs[1] + 'C']
        end

        pageIndex = drawingAndSheetLookup[refs]
        if !pageIndex.nil?

          # rectangle with no border and opacity of 0.2.
          @boxes << @doc.add({Type: :Annot, Subtype: :Square, Rect: objRect,
                                F: 0b000, C: C::LINK_COLOR, Border: [0, 0, 0],
                                IC: C::LINK_COLOR, CA: 0.1})

          # hyperlink
          @newLinks << @doc.add({Type: :Annot, Subtype: :Link, Rect: objRect, F: 0b000,
                               A: { Type: :Action, S: :GoTo,
                                    D: [@doc.pages[pageIndex], :XYZ, nil, nil, nil] },
                               C: C::LINK_COLOR, Border: [0, 0, 0]})
        else
          puts 'A new page link not found: ' + match.to_s + \
               ' Key: ' + text.to_s if @verbose
        end

      end
    end

    # we may not start with any annotations on TTF drawings
    page[:Annots] = page[:Annots] || []

    # add rectangle annotations to list
    # add first to have "below" the link annotations
    @boxes.each do |box|
      page[:Annots] << box
    end

    # add rectangles on links
    if @canvasLinks
      @newLinks.each do |link|
        # add link annotations to list
        page[:Annots] << link
        # create rectangle
        @canvas.stroke_color = C::LINK_RECT_COLOR
        @canvas.rectangle(link[:Rect].left, link[:Rect].bottom,
                          link[:Rect].width, link[:Rect].height,
                          radius: C::RECT_RADIUS)
        @canvas.stroke
      end
    else
      # add link annotations to list
      @newLinks.each do |link|
        page[:Annots] << link
      end
    end

  end
end
