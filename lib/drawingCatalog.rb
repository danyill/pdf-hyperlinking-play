# encoding: utf-8
# frozen_string_literal: true

require 'hexapdf'

require_relative 'drawing'

class DrawingCatalog
  attr_reader :rungLookup, :drawingAndSheetLookup, :drawings

  def initialize(fileName, newName: false, debug: false,
                 verbose: false, dryrun: false, canvasLinks: false)
    @debug = debug
    @verbose = verbose
    @dryrun = dryrun
    @newName = newName
    @canvasLinks = canvasLinks

    @rungLookup = {}
    @drawingAndSheetLookup = {}
    @fileName = fileName

    puts @fileName if @verbose

    begin
      @doc = HexaPDF::Document.open(@fileName)
    rescue StandardError
      puts 'ERROR: Unable to open file: ' + @fileName.to_s
      exit 1
    end

    @drawings = {}

    initializeDrawings
    createIndexes
    puts @drawingAndSheetLookup if @verbose
    createLinks

    filepath = File.expand_path(fileName)

    if @newName == false || @newName == nil
      ext = File.extname(fileName)
      @outputFile  = File.join(File.dirname(filepath), File.basename(filepath, ext) + '_links' + ext)
    else
      @outputFile = File.expand_path(@newName)
    end

    begin
      @doc.write(@outputFile, validate: false, optimize: true) unless @dryrun
    rescue HexaPDF::MalformedPDFError
      puts 'ERROR: Unable to write PDF: ' + @outputFile.to_s + ' -- Malformed data in PDF'
    rescue StandardError => e
      puts "ERROR: Uncaught error during processing: #{$!}"
      puts "Backtrace:\n\t#{e.backtrace.join("\n\t")}"
      exit 1
    end

    puts 'Wrote: ' + @outputFile if @verbose
  end

  def initializeDrawings
    @doc.pages.each do |page|
      puts 'Page ' + (page.index+1).to_s if @verbose
      @drawings[page.index] = Drawing.new(@doc, page, @debug, @verbose, @canvasLinks, @fileName)
    end
  end

  def createIndexes
    @drawings.each_value do |d|
      @drawingAndSheetLookup[[d.drawingNum, d.sheet]] = d.index
    end

    @drawings.values.each do |d|
      d.rungs.keys.each do |r|
        # only catalog a rung if it has not already been catalogged
        # for the same drawing number. Improves resilience for capturing
        # border numbers instead of rung numbers
        if !@rungLookup.key? [d.drawingNum, r]
          @rungLookup[[d.drawingNum, r]] = [d.index, d.rungs[r][0], d.rungs[r][1], d.rungs[r][2]]
        end
      end
    end
  end

  def createLinks
    @drawings.values.each do |d|
      d.makeLinks(@rungLookup, @drawingAndSheetLookup, @drawings)
      puts 'For page ' + (d.index + 1).to_s + ': ' + d.newLinks.length.to_s + ' links made.' if @verbose
    end
  end

end
